package watchme.app.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatcherDetails {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private Integer phone;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The phone
     */
    public Integer getPhone() {
        return phone;
    }

    /**
     * @param phone The phone
     */
    public void setPhone(Integer phone) {
        this.phone = phone;
    }

}
