package watchme.app.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripDetails {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("sourceAddr")
    @Expose
    private String sourceAddr;
    @SerializedName("destAddr")
    @Expose
    private String destAddr;
    @SerializedName("sourceLatlong")
    @Expose
    private String sourceLatlong;
    @SerializedName("destLatlong")
    @Expose
    private String destLatlong;
    @SerializedName("watchers")
    @Expose
    private String watchers;

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The sourceAddr
     */
    public String getSourceAddr() {
        return sourceAddr;
    }

    /**
     *
     * @param sourceAddr
     * The sourceAddr
     */
    public void setSourceAddr(String sourceAddr) {
        this.sourceAddr = sourceAddr;
    }

    /**
     *
     * @return
     * The destAddr
     */
    public String getDestAddr() {
        return destAddr;
    }

    /**
     *
     * @param destAddr
     * The destAddr
     */
    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    /**
     *
     * @return
     * The sourceLatlong
     */
    public String getSourceLatlong() {
        return sourceLatlong;
    }

    /**
     *
     * @param sourceLatlong
     * The sourceLatlong
     */
    public void setSourceLatlong(String sourceLatlong) {
        this.sourceLatlong = sourceLatlong;
    }

    /**
     *
     * @return
     * The destLatlong
     */
    public String getDestLatlong() {
        return destLatlong;
    }

    /**
     *
     * @param destLatlong
     * The destLatlong
     */
    public void setDestLatlong(String destLatlong) {
        this.destLatlong = destLatlong;
    }

    /**
     *
     * @return
     * The watchers
     */
    public String getWatchers() {
        return watchers;
    }

    /**
     *
     * @param watchers
     * The watchers
     */
    public void setWatchers(String watchers) {
        this.watchers = watchers;
    }

}