package watchme.app;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.firebase.client.ChildEventListener;
import com.firebase.client.Firebase;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import watchme.app.Models.TripDetails;
import watchme.app.Models.WatcherDetails;
import watchme.app.MultipleContactsPicker.ContactData;
import watchme.app.MultipleContactsPicker.ContactPickerActivity;
import watchme.app.utilities.Util;

public class MainActivity extends AppCompatActivity implements com.google.android.gms.location.LocationListener, RoutingListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    protected GoogleMap map;
    protected LatLng start;
    protected LatLng end;
    protected LatLng currentLoc;
    private LocationGooglePlayServicesProvider provider;
    String watchersList = "";

    // flag for GPS Tracking is enabled
    boolean isGPSTrackingEnabled = false;

    LocationListener locationListener;

    final int REQUEST_CODE = 100;
    @InjectView(R.id.cbCheckAll)
    CheckBox cbCheckAll;

    @InjectView(R.id.start)
    TextView starting;
    @InjectView(R.id.destination)
    AutoCompleteTextView destination;
    @InjectView(R.id.send)
    Button send;
    @InjectView(R.id.feelingUnsafeBtn)
    Button feelingUnsafe;
    @InjectView(R.id.callPolice)
    Button callPolice;
    @InjectView(R.id.directionCardLay)
    LinearLayout directionCardLayout;
    @InjectView(R.id.startTripLayout)
    RelativeLayout startTripLayout;
    @InjectView(R.id.controlsLayout)
    LinearLayout controlsLayout;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.endTrip)
    FloatingActionButton endTrip;

    private String LOG_TAG = "MyActivity";

    private PlaceAutoCompleteAdapter mAdapter;
    private ProgressDialog progressDialog;
    private Polyline polyline;
    private static final int GET_PHONE_NUMBER = 3007;
    private Marker marker;
    ArrayList<WatcherDetails> selectedWatchers = new ArrayList<WatcherDetails>();

    String url = "https://watchme-app-demo.firebaseio.com/data";
    String tripDetails = "https://watchme-app-demo.firebaseio.com/Trips";
    Firebase mFirebaseRef;
    Firebase mFirebaseRef2;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;
    ChildEventListener childListner;

    LocationManager locationManager;

    int PLACE_PICKER_START = 1;
    int PLACE_PICKER_END = 2;


    protected static final String TAG = "location-updates-sample";

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /*// Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";*/

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;


    private static final LatLngBounds BOUNDS_JAMAICA = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
            new LatLng(72.77492067739843, -9.998857788741589));


    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
            //Toast.makeText(getBaseContext(), "onResume Connected", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(getBaseContext(), "onResume NotConnected", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * This activity loads a map and then displays the drawRoute and pushpins on it.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        //toolbar.inflateMenu(R.menu.menu_main);

        Firebase.setAndroidContext(this);
        mFirebaseRef.getDefaultConfig().setPersistenceEnabled(true);
        mFirebaseRef2.getDefaultConfig().setPersistenceEnabled(true);
        mFirebaseRef = new Firebase(url);
        mRequestingLocationUpdates = false;
        buildGoogleApiClient();

        MapsInitializer.initialize(this);
        mGoogleApiClient.connect();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        }
        map = mapFragment.getMap();


        endTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EndTripAlertDialog();
            }
        });

        /*mAdapter = new PlaceAutoCompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_JAMAICA, null);*/


        /*
        * Updates the bounds being used by the auto complete adapter based on the position of the
        * map.
        * */
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
                LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
                mAdapter.setBounds(bounds);
            }
        });


        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(18.013610, -77.498803));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        map.moveCamera(center);
        map.animateCamera(zoom);


        /*
        * Adds auto complete adapter to both auto complete
        * text views.
        * */
        //starting.setAdapter(mAdapter);
        destination.setAdapter(mAdapter);

        starting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(getApplicationContext(), R.string.starting_point, Toast.LENGTH_LONG).show();
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    Intent intent = intentBuilder.build(MainActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_START);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.d("errorrrr", "" + e);
                    GooglePlayServicesUtil
                            .getErrorDialog(e.getConnectionStatusCode(), MainActivity.this, 0);
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(getApplicationContext(), "Google Play Services is not available.",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        /*
        * Sets the start and destination points based on the values selected
        * from the autocomplete text views.
        * */

        /*starting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);

            *//*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              *//*
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request did not complete successfully
                            Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Get the Place object from the buffer.
                        final Place place = places.get(0);

                        start = place.getLatLng();
                    }
                });

            }
        });*/
        destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i(LOG_TAG, "Autocomplete item selected: " + item.description);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request did not complete successfully
                            Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Get the Place object from the buffer.
                        final Place place = places.get(0);

                        end = place.getLatLng();
                        startTripLayout.setVisibility(View.VISIBLE);
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
                            //Toast.makeText(getBaseContext(), "Destination :- Already Connected", Toast.LENGTH_LONG).show();
                        } else {
                            mGoogleApiClient.connect();

                            //Toast.makeText(getBaseContext(), "Destination :-Connecting..", Toast.LENGTH_LONG).show();

                        }
                    }
                });

            }
        });

    }


    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        //Toast.makeText(getBaseContext(), "onStart NotConnected", Toast.LENGTH_LONG).show();

    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
            //Toast.makeText(getBaseContext(), "onResume Connected", Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(getBaseContext(), "onResume NotConnected", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.feelingUnsafeBtn)
    public void notifyFeelingUnsafe() {
        //Toast.makeText(getBaseContext(), "We will inform this location to POLICE and make this place safe", Toast.LENGTH_LONG).show();
        SimpleAlertDialog(R.string.unsafe_msg);
    }

    @OnClick(R.id.callPolice)
    public void callPolice() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:100"));
        startActivity(callIntent);
    }

    @OnClick(R.id.send)
    public void sendRequest() {
        if (start != null && destination != null) {

            if (Util.Operations.isOnline(this)) {
                //Close soft keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                directionCardLayout.setVisibility(View.GONE);
                controlsLayout.setVisibility(View.VISIBLE);
                startTripLayout.setVisibility(View.GONE);
                showContacts();
                drawRoute();
                endTrip.setVisibility(View.VISIBLE);
                endTrip.setIcon(R.drawable.ic_clear_white_24dp);
                endTrip.setColorNormal(Color.parseColor("#4cd0e0"));
                endTrip.setTitle("END TRIP");

                //endTrip.setBackgroundColor(Color.parseColor("#448AFF"));
            } else {
                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.choose_startend, Toast.LENGTH_SHORT).show();
        }
    }

    private void showContacts() {
        Intent contactPicker = new Intent(MainActivity.this, ContactPickerActivity.class);
        contactPicker.putExtra(ContactData.CHECK_ALL, false);
        contactPicker.setPackage("watchme.app");
        startActivityForResult(contactPicker, REQUEST_CODE);
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("Building", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
        createLocationRequest();
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    public void drawRoute() {
        if (start == null || end == null) {
            if (start == null) {
               /* if (starting.getText().length() > 0) {
                    starting.setError("Choose location from dropdown.");
                } else {
                    Toast.makeText(this, "Please choose a starting point.", Toast.LENGTH_SHORT).show();
                }*/
                Toast.makeText(this, R.string.starting_point, Toast.LENGTH_SHORT).show();
            }
            if (end == null) {
                /*if (destination.getText().length() > 0) {
                    destination.setError("Choose location from dropdown.");
                } else {
                    Toast.makeText(this, "Please choose a destination.", Toast.LENGTH_SHORT).show();
                }*/
                Toast.makeText(this, R.string.destination_point, Toast.LENGTH_SHORT).show();
            }
        } else {
            progressDialog = ProgressDialog.show(this, "Please wait...",
                    "Fetching route information.", true);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .waypoints(start, end)
                    .build();
            routing.execute();
        }
    }


    @Override
    public void onRoutingFailure() {
        // The Routing request failed
        progressDialog.dismiss();
        Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {
        // The Routing Request starts
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {

        progressDialog.dismiss();
        map.animateCamera(CameraUpdateFactory.newLatLng(start));
        /*if(start.latitude>end.latitude){
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(end, start), 20));
        }else {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(new LatLngBounds(start, end), 20));
        }*/

        if (polyline != null)
            polyline.remove();


        polyline = null;
        //adds drawRoute to the map.
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.material_deep_teal_500));
        polyOptions.width(10);
        polyOptions.addAll(mPolyOptions.getPoints());
        polyline = map.addPolyline(polyOptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.finish));
        map.addMarker(options);
    }


    @Override
    public void onRoutingCancelled() {
        Log.i(LOG_TAG, "Routing was cancelled.");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("resulut", "result");
        if (requestCode == PLACE_PICKER_START) {
            Log.d("resulut", "result");

            if (resultCode == RESULT_OK) {
                final Place place = PlacePicker.getPlace(data, this);
                Toast.makeText(getApplicationContext(), "" +
                        place.getAddress(), Toast.LENGTH_LONG).show();
                if (place.getLatLng().toString() != null) {
                    start = place.getLatLng();
                    currentLoc = place.getLatLng();
                        starting.setText(start.toString());
                    if (mGoogleApiClient.isConnected()) {
                        destination.setAdapter(mAdapter);
                        //Toast.makeText(getBaseContext(), "its connected", Toast.LENGTH_LONG).show();
                    } else {
                        mGoogleApiClient.connect();
                        //Toast.makeText(getBaseContext(), "Connecting...", Toast.LENGTH_LONG).show();

                    }

                    //send.setBackgroundColor(Color.parseColor("#32CD32"));
                }
            }
        } else if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(ContactData.CONTACTS_DATA)) {
                    ArrayList<ContactData> contacts = data.getParcelableArrayListExtra(ContactData.CONTACTS_DATA);

                    if (contacts != null) {

                        Iterator<ContactData> iterContacts = contacts.iterator();
                        while (iterContacts.hasNext()) {
                            ContactData contact = iterContacts.next();
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(contact.phoneNmb, null, "http://watchme.neocities.org/index.html", null, null);
                            watchersList += contact.firstname + " " + contact.lastname + " " + contact.phoneNmb + " " + contact.email + "\n";
                        }
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
                            //Toast.makeText(getApplicationContext(), "Contact:- Already connected", Toast.LENGTH_LONG).show();
                        } else {
                            mGoogleApiClient.connect();
                            //Toast.makeText(getApplicationContext(), "Contact:- Connecting..", Toast.LENGTH_LONG).show();
                        }


                    }
                }
            }
            writeTripDetails();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void writeTripDetails() {
        Toast.makeText(getApplicationContext(), "writing data to firebase", Toast.LENGTH_LONG).show();
        mFirebaseRef2 = new Firebase(tripDetails);
        SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-yyyy_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        TripDetails trip = new TripDetails();
        trip.setDate(currentDateandTime);
        trip.setSourceAddr("" + start);
        trip.setDestAddr(destination.getText().toString());
        trip.setSourceLatlong("" + start.latitude + "," + start.longitude);
        trip.setDestLatlong("" + end.latitude + "," + end.longitude);
        trip.setWatchers(watchersList);

        Firebase ref = mFirebaseRef2.child("Shiv" + System.currentTimeMillis() + "");
        ref.setValue(trip);
        //mFirebaseRef2 = null;
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.v(LOG_TAG, connectionResult.toString());
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
        mAdapter = new PlaceAutoCompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_JAMAICA, null);
        destination.setAdapter(mAdapter);
        //Toast.makeText(getBaseContext(), "adapter set", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onLocationChanged(Location location) {

        //Toast.makeText(getBaseContext(), "called update", Toast.LENGTH_LONG).show();
        //Remove priveous marker
        if (marker != null) {
            marker.remove();
        }
        if (currentLoc != null) {

            MarkerOptions currentLocMarker = new MarkerOptions();
            //LatLng latLng = new LatLng(Double.parseDouble(lats.get(count)), Double.parseDouble(lons.get(count)));
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            //currentLocMarker.position(new LatLng(location.getLatitude(), location.getLongitude()));
            currentLocMarker.position(latLng);
            //currentLocMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cast_light));
            currentLocMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.current));
            marker = map.addMarker(currentLocMarker);

            currentLoc = latLng;

            //starting.setText(start.toString());
                        /*CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(23);

                        map.moveCamera(center);
                        map.animateCamera(zoom, 2000, null);*/

            //count++;

            try {
                //Thread.sleep(1000);
                Firebase ref = mFirebaseRef.child("Shiv" + System.currentTimeMillis() + "");
                ref.setValue("{" + latLng.toString() + "}");
                Log.d("Dataadded", "{" + latLng.toString() + "}");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_endTrip) {
            /*stopTrip();
            item.setVisible(false);
            invalidateOptionsMenu();*/

            //MainActivity.this.recreate();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void stopTrip() {
        directionCardLayout.setVisibility(View.VISIBLE);
        controlsLayout.setVisibility(View.GONE);
        endTrip.setVisibility(View.GONE);
        starting.setText("");
        destination.setText("");
        start = null;
        end = null;
        if (map != null) {
            map.clear();
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(18.013610, -77.498803));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
            map.moveCamera(center);
            map.animateCamera(zoom);
        }
        if (marker != null) {
            marker.remove();
        }
        mFirebaseRef = null;
        mFirebaseRef2 = null;
    }


    /**
     * Function to show settings alert dialog
     */
    public void showEnableSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle(R.string.location_on_msg);
        //alertDialog.setMessage("Please turn on Location");
        //On Pressing Setting button
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                MainActivity.this.startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //mGotLocation=false;
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void CloseAppAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle(R.string.message_close);
        //On Pressing Setting button
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopTrip();
                finish();
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //mGotLocation=false;
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    public void EndTripAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle(R.string.message_close);
        //On Pressing Setting button
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                stopTrip();
                //finish();
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //mGotLocation=false;
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    public void SimpleAlertDialog(int desc) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle(R.string.message);
        alertDialog.setMessage(desc);
        //On Pressing Setting button
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        alertDialog.show();
    }




    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //Toast.makeText(getApplicationContext(), "Would you like to END session?", Toast.LENGTH_LONG).show();
        CloseAppAlertDialog();
    }
}